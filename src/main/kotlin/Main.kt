import kotlinx.coroutines.*
fun firstNum(): Int{
    var num = 0
    runBlocking {
        val job = GlobalScope.launch {
            delay(5000L)
            num = (0..10).random()
        }
        job.join()
    }
    return num
}
fun secondNum(): Int {
    var num = 0
    runBlocking {
        val job = GlobalScope.launch {
            delay(5000L)
            num = (100..110).random()
        }
        job.join()
    }
    return num
}
fun waitAndQuit() = runBlocking {
    GlobalScope.launch {
        repeat(3) { i ->
            println("I'm sleeping $i ...")
            delay(800L)
        }
        println("main: I'm tired of waiting! I'm running finally")
        delay(800L)
        println("main: Now I can quit.")
    }
}
fun printHelloWorld(){
    GlobalScope.launch{
        delay(1000L)
        println("World!")
    }
    println("Hello,")
    Thread.sleep(2000L)
}
fun menu(){
    println(""" Menu:
        |1. Print Hello World!
        |2. Get the sum of the results of two function
        |3. Make the system wait and quit
    """.trimMargin())
}
fun main() {
    var number1: Int
    var number2: Int
    while (true) {
        menu()
        println("Enter your choice: ")
        var choice: String = readLine().toString()
        when (choice) {
            "1" -> printHelloWorld()
            "2" -> {
                println("Synchronously:")
                number1 = firstNum()
                number2 = secondNum()
                println("If you add $number1 to $number2 you get ${number1 + number2}")
                println("Asynchronously:")
                GlobalScope.launch {
                    number1 = firstNum()
                }
                GlobalScope.launch {
                    number2 = secondNum()
                }
                runBlocking { delay(5500L) }
                println("If you add $number1 to $number2 you get ${number1 + number2}")
                // В первом случае программа последовательно вызывала первую и вторую функцию, тратя по 5 секунд на каждую
                // Во втором случае функции вызывались в сопрограммах, пока выполнялась основная программа и потратилось 5 секунд
                // На оба вызова сразу, поэтому времени было потрачено в 2 раза меньше
            }
            "3" -> {
                waitAndQuit()
                runBlocking { delay(4000L) }
                break
            }
            else -> println("Wrong Input!")
        }
    }
}
